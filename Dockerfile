FROM gcr.io/kaniko-project/executor:debug

# make image compatible with bitbucket CI
RUN [ \
  "/busybox/sh", \
  "-c", \
  "mkdir -p /bin /usr/bin && \
  bin_symlinks='sh echo cat' && \
  set -- $bin_symlinks && while [ -n \"$1\" ]; do ln -sf /busybox/$1 /bin; shift; done && \
  usr_bin_symlinks='mkfifo' && \
  set -- $usr_bin_symlinks && while [ -n \"$1\" ]; do ln -sf /busybox/$1 /usr/bin; shift; done" \
]

# make image eligible as bitbucket CI pipe
COPY common.sh README.md /
COPY pipe /
RUN chmod +x /pipe.sh

ENTRYPOINT ["/pipe.sh"]
