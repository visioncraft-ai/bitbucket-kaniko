[Kaniko](https://github.com/GoogleContainerTools/kaniko) debug image (includes shell & busybox) modified for Bitbucket CI pipelines.

[visioncraftai/bitbucket-kaniko](https://hub.docker.com/repository/docker/visioncraftai/bitbucket-kaniko) on Dockerhub

Inspired by [npm-publish pipe](https://bitbucket.org/atlassian/npm-publish/src/master/).


## Why ##

In Bitbucket CI `entrypoint` cannot be specified and `/bin/sh` is used for all steps. Original Kaniko debug image has shell in `/busybox/sh` and location `/bin/sh` doesn't exists. This results in an error.

## Usage ##

Use `bitbucket-kaniko` image, set environment variables and run script in your repository (it's cleaner than escaping string in `bitbucket-pipelines.yml`).

This image can be also used as pipe (work in progress).

### Example building and pushing to dockerhub ###

Add build step: `bitbucket-pipelines.yml`:
```
- step:
    image: visioncraftai/bitbucket-kaniko:debug
    script:
      - export IMAGE_REPOSITORY_URI=visioncraftai/bitbucket-kaniko
      - export DOCKERFILE_PATH=$BITBUCKET_CLONE_DIR/Dockerfile
      - export CONTEXT=$BITBUCKET_CLONE_DIR
      - export IMAGE_TAG="latest"
      - /bin/sh ./.bitbucket/ci/kaniko.sh
```

`.bitbucket/ci/kaniko.sh`:
```
echo "{\"auths\":{\"https://index.docker.io/v1/\":{\"auth\":\"xxxxxxxxxxxxxxx\"}}}" > /kaniko/.docker/config.json

/kaniko/executor --context "$CONTEXT" --dockerfile "$DOCKERFILE_PATH" --destination "$IMAGE_REPOSITORY_URI:$IMAGE_TAG"

# bitbucket CI hangs if /kaniko/executor is the last command, explicitly exit with 'exit code' from executor
exit $?
```

Where `xxxxxxxxxxxxxxx` is base64 encoded docker user and password: `echo -n USER:PASSWORD | base64`.

Check out `bitbucket-pipelines.yml` in this repository to see full working example.

### Pushing to other repositories ###

Refer to official Kaniko [documentation](https://github.com/GoogleContainerTools/kaniko#pushing-to-different-registries) and edit `.bitbucket/ci/kaniko.sh` accordingly.


### Kaniko executor image ###

In order to keep up to date with original kaniko executor image, rebuild is scheduled every day and base image `gcr.io/kaniko-project/executor:debug` is used.

Image tag `latest` is used for convenience and is always the same as `debug`.
